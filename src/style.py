def customs() -> dict[str, str]:
    return {
        "primary": "#9cdf55",
        "secondary": "#6aa556",
        "warning": "#b4e48c",
        "warn": "#805827",
        "bgColor": "#c9daba",
        "textColorGray": "#91a398",
        "textColor": "#e9e0d1",
    }
