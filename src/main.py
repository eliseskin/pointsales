from flet import Page, CrossAxisAlignment
# from utils.logging import logger
from views.login import Login
from style import customs


def main(page: Page):
    page.title = "La Palmita"
    page.bgcolor = customs()["bgColor"]
    page.horizontal_alignment = CrossAxisAlignment.CENTER

    def route_change(e):
        page.clean()

        if page.route == "/":
            page.add(Login(page))
        elif page.route == "/users":
            pass
        elif page.route == "/panel":
            pass
        elif page.route == "/products":
            pass
        elif page.route == "/orders":
            pass
        elif page.route == "/clients":
            pass

    page.on_route_change = route_change
    page.go(page.route)
    print(page.route)
