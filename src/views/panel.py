from flet import Container, Page


class Panel(Container):
    def __init__(self, page: Page):
        super().__init__()

        page.update()
