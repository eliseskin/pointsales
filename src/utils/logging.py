import logging
import datetime as dt


def logger() -> logging.Logger:
    logger = logging.getLogger('APP')
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(
        filename=f"logs/{dt.datetime.now().month}.log", encoding='utf-8')

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s [%(levelname)s]:  %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    return logger
