import sqlite3


class Connection(object):
    db_name = "database/palmita.db"

    def run_query(self, sql: str, parameters=()):
        with sqlite3.connect(self.db_name) as conn:
            cursor = conn.cursor()
            result = cursor.execute(sql, parameters)
            conn.commit()
            conn.close()
        return result
